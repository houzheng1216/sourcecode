package com.hou.lang;

import sun.misc.Cleaner;
import sun.misc.JavaLangRefAccess;
import sun.misc.SharedSecrets;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

public class MyTest02 {
    // 类对象
    class UserTest {
        // 模拟内存占用3M，以更好观察gc前后的内存变化
        private byte[] memory = new byte[3*1024*1024];
    }
    /**
     * 测试弱引用在内存足够时不会被GC，在内存不足时才会被GC的特性
     * JVM参数 -Xms10m -Xmx10m -XX:+PrintGCDetails  将内存大小限制在20M，并打印出GC日志
     */
    public void testSoftReference(){
        // 创建弱引用类，将该引用绑定到弱引用对象上
        SoftReference<UserTest> sortRet = new SoftReference<>(new UserTest());
        // 此时并不会被GC.内存足够
        System.gc();
        System.out.println("GC后通过软引用重新获取了对象：" + sortRet.get());

        // 模拟内存不足，即将发生OOM时,软引用会被回收,获取为null
        List<UserTest> manyUsers = new ArrayList<>();
        for(int i = 1; i < 100000; i++){
            System.out.println("将要创建第" + i + "个对象");
            manyUsers.add(new UserTest());
            System.out.println("创建第" + i + "个对象后, 软引用对象：" + sortRet.get());
        }
    }

    public static void main(String[] args) {
        MyTest02 referenceTest = new MyTest02();
        referenceTest.testSoftReference();
    }
}
