package com.hou.lang;


import org.junit.Test;

import javax.xml.stream.events.Characters;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class MyTest01 {

    public static void main(String[] args) throws InterruptedException {
        // 设置所有线程
        // Thread.setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler());

        Thread thread = new Thread(() -> {
            System.out.println("程序start");
            //每个线程单独设置，如果没有，默认获取所有默认的
            Thread.currentThread().setUncaughtExceptionHandler((x, y) -> {
                System.out.println(x.getName());
                System.out.println(y.getMessage());
            });
            // 这种就是未检查异常，并没有trycatch，是代码逻辑问题,直接抛异常会导致程序结束
            Integer.parseInt("h哈哈");
        });
        thread.start();
        Thread.sleep(1000);
        System.out.println("程序继续111");
        // 获取所有线程轨迹，key线程，value轨迹map，即执行方法倒序，最后执行方法的在最前面
        Thread.getAllStackTraces().forEach((key, value) -> {
            System.out.println(key);
            System.out.println(Arrays.toString(value));
        });
        // 获取当前线程轨迹
        System.out.println("-----------------");
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        System.out.println(Arrays.toString(stackTrace));


        Character character = '0';
        Character character1 = '1';
        Character[] characters = {character, character1};
        char[] chars = {'0', '1'};
        List<Character> characters1 = Arrays.asList(characters);
        List<char[]> chars1 = Arrays.asList(chars);
        Arrays.asList(characters).forEach(x -> {
            int i = '0' + x;
        });
    }

    @Test
    public void test02() {
        AtomicInteger atomicInteger = new AtomicInteger();
        System.out.println(atomicInteger.get());
        System.out.println(atomicInteger.getAndAdd(2));
        System.out.println(atomicInteger.getAndAdd(5));
        System.out.println(atomicInteger.getAndAdd(5));
        ThreadLocal<Integer> integerThreadLocal = ThreadLocal.withInitial(() -> 5);
        System.out.println(integerThreadLocal.get());
        ThreadLocal<String> stringThreadLocal = new ThreadLocal<>();
        int i=16;
        int i1 = i >>> 2;
        int i2 = i1 >>> 2;
        int i3 = i2 >>> 2;
        int i4 = i3 >>> 2;
        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i3);
        System.out.println(i4);
    }
}
