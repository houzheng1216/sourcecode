package com.hou.lang;

import java.io.*;

public class EnumTest {

    enum MyCode{
        ONE("1","编码一"),
        TWO("2","编码二");

        private String code;
        private String name;

        MyCode(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    public static void main(String[] args) {
        // 获取一个枚举实例
        MyCode one = MyCode.valueOf(MyCode.class, "ONE");
        // 可以调用Enum类中的实例方法
        one.compareTo(MyCode.TWO);
    }
}
