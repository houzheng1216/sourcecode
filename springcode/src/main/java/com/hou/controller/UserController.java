package com.hou.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {
    @ResponseBody
    @GetMapping("/test")
    public String getUser(){
        return "user";
    }

    @GetMapping("/index")
    public String index(){
        return "user";
    }
}
