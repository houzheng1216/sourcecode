package com.hou.bean;

import org.springframework.stereotype.Component;
import java.math.BigInteger;

@Component
public class Person {
    private BigInteger id;
    private String name="test";

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
