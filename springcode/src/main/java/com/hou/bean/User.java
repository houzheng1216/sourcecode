package com.hou.bean;

import java.math.BigInteger;
import java.util.Random;

public class User {
    private BigInteger id=new BigInteger(5,20,new Random());
    private String name="user";

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
