package com.hou.spring;

import com.hou.bean.Person;
import org.junit.Test;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanTest{
    @Test
    public void beanTest(){
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-bean.xml");
        //ApplicationContext没有实现接口,但是可以通过方法直接获取使用
        AutowireCapableBeanFactory autowireCapableBeanFactory = applicationContext.getAutowireCapableBeanFactory();
        // autowireCapableBeanFactory创建Bean实例,执行多次就创建多个
        Person person = (Person) autowireCapableBeanFactory.createBean(Person.class, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        //没有@Autowired注解也直接给注入了
        //System.out.println("获取自动注入的属性:"+person.getUser());
        //异常: No qualifying bean of type 'com.hou.spring.Person' available
//        Person bean = applicationContext.getBean(Person.class);//没有交给spring容器管理
//        System.out.println(bean);
    }
}
