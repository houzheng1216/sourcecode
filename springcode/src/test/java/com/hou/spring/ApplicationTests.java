package com.hou.spring;

import com.hou.bean.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ApplicationTests {
    @Autowired
    ApplicationContext applicationContext;

    @Test
    public void contextLoads() {
        Person person = (Person) applicationContext.getBean("person");
        //person.setName("hah");
        System.out.println(person.getName());


    }


}
